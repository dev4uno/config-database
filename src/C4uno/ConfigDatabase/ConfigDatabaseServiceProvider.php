<?php

declare(strict_types=1);

namespace C4uno\ConfigDatabase;

use C4uno\ConfigDatabase\Models\Config;
use Illuminate\Support\ServiceProvider;

/**
 * Class ConfigDatabaseServiceProvider
 * @package C4uno\Payment
 */
class ConfigDatabaseServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadConfigFromDB();

        $version = $this->getVersion();

        if ($version >= 5.3) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        } else {
            $this->publishes([
                __DIR__.'/database/migrations/' => database_path('migrations')
            ], 'migrations');

            $this->publishes([
                __DIR__.'/config/config_database.php' => config_path('config_database.php')
            ], 'config');
        }
    }

    /**
     * @param Config $config
     * @return string
     */
    private function getSection(Config $config): string
    {
        $parts = explode('.', $config->key);

        return $parts[0];
    }

    /**
     * @param Config $config
     * @return mixed
     */
    private function parseValue(Config $config)
    {
        switch ($config->type) {
            case Config::BOOLEAN:
                return boolval($config->value);
            case Config::INTEGER:
                return intval($config->value);
            case Config::STRING:
                return $config->value;
            case Config::ARRAY:
                return json_decode($config->value, true);
            default:
                throw new \InvalidArgumentException('The type '.$config->type.' does not exists.');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     *
     */
    private function loadConfigFromDB()
    {
        $sections = config('config_database.sections');
        /** @var  \Illuminate\Database\Schema\Builder $schemaBuilder */
        $schemaBuilder = app('db')->getSchemaBuilder();

        if (!$schemaBuilder->hasTable(config('config_database.table_name'))) {
            return;
        }

        foreach (Config::all() as $config) {
            $section = $this->getSection($config);

            if (in_array($section, $sections)) {
                $value = $this->parseValue($config);
                config()->set($config->key, $value);
            }
        }
    }

    private function getVersion(): float
    {
        $version = floatval($this->app->version());

        // Version does not detected
        if ($version !== 0.0) {
            return $version;
        }

        $sub = substr(
            $this->app->version(),
            strlen('lumen ('),
            6
        );

        return floatval($sub);
    }
}
