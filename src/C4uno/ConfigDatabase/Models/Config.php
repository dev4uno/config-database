<?php
declare(strict_types=1);

namespace C4uno\ConfigDatabase\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Config
 * @property string $key
 * @property string $value
 * @property string $type
 * @package C4uno\ConfigDatabase\Models
 */
class Config extends Model
{
    const BOOLEAN = 'boolean';
    const INTEGER = 'integer';
    const STRING = 'string';
    const ARRAY = 'array';
    const DATA_TYPES = [
        self::BOOLEAN,
        self::INTEGER,
        self::STRING,
        self::ARRAY,
    ];
    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = config('config_database.table_name');
    }
}
