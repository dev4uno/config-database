# Config Database

It reads config values from database and merges them with the configuration files. So that, you can changed in run-time.


## Install

```bash
composer require c4uno/config-database
```

### Laravel < 5.3

Add the service provider to `config/app.php`

```php
    C4uno\ConfigDatabase\ConfigDatabaseServiceProvider::class,
```

Publish the migrations

```bash
artisan vendor:publish --provider="C4uno\ConfigDatabase\ConfigDatabaseServiceProvider" --tag="migrations"

```

Publish configuration

```bash
artisan vendor:publish --provider="C4uno\ConfigDatabase\ConfigDatabaseServiceProvider" --tag="config"
```

## Lumen

Register the service provider in `bootstrap/app.php`

```php
$app->register(C4uno\ConfigDatabase\ConfigDatabaseServiceProvide);
```

## Use

By default the package use the configuration in the files `app` and `emails`. If you want use other files then
modify the list in the `sections` key in `config/config_database.php`.

Add a new row the table `configs` with the config key, you datatype (see the `C4uno\ConfigDatabase\Models\Config` class to a list of the data types available)
For example, to change the value of the `subdomains.panel` in the `c4uno` configuration file you have to insert the 
following row:

```sql
INSERT INTO configs VALUE (1, 'c4uno.subdomains.panel','panel.local.4uno.org','string', CURRENT_DATE(), CURRENT_DATE())
```


## Implemented
* The `config()` helper must go on working
* The values in the configuration files must be the default values.
* Specific what configs should be in the DB. Because does not the configuration should be available.
* Specific the table name where the settings should be saved.
* Support more datatypes. Array are saved as json so heterogeneous elements are possible.
* Compatible with Laravel < 5.7 and Laravel 5.1

## TODO
* Allow long values (more than 255).
* Be aware of the performance. Use cache.
